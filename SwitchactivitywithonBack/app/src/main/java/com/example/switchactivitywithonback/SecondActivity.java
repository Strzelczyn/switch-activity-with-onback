package com.example.switchactivitywithonback;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.switchactivitywithonback.databinding.ActivitySecondBinding;

public class SecondActivity extends AppCompatActivity {

    private ActivitySecondBinding activitySecondBinding;

    private String outputtext;

    public String getOutputtext() {
        return outputtext;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        outputtext = getResources().getString(R.string.output_text)+ " " + getIntent().getStringExtra("inputtext");
        activitySecondBinding = DataBindingUtil.setContentView(this, R.layout.activity_second);
        activitySecondBinding.setItem2(this);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("outputtext", outputtext);
        setResult(2, intent);
        super.onBackPressed();
    }
}
