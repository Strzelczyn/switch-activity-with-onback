package com.example.switchactivitywithonback;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.switchactivitywithonback.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding activityMainBinding;

    private String outputtext;

    private String inputtext;

    public String getOutputtext() {
        return outputtext;
    }

    public String getInputtext() {
        return inputtext;
    }

    public void setInputtext(String inputtext) {
        this.inputtext = inputtext;
    }

    public void switchActivty() {
        Intent i = new Intent(this, SecondActivity.class);
        i.putExtra("inputtext", inputtext);
        startActivityForResult(i, 2);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        activityMainBinding.setItem(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            outputtext = data.getStringExtra("outputtext");
            /* databinding can't auto update view
             * to work properly must be used notifyPropertyChanged
             * but MainActivity can't extends from BaseObservable
             */
            TextView z = this.findViewById(R.id.textView);
            z.setText(outputtext);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
